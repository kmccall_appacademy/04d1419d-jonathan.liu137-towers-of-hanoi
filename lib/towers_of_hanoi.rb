class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    while !won?
      self.render
      print "Choose a tower to move the top disk from: "
      from_tower = gets.to_i
      print "Choose a tower to move the disk to: "
      to_tower = gets.to_i
      puts ""

      if valid_move?(from_tower, to_tower)
        move(from_tower, to_tower)
      else
        puts "Illegal move. Please choose another move."
        puts ""
        next
      end
    end

    self.render
    puts "Congratulations! You have won the game!"
  end

  def render
    level = 2

    while level > -1
      tower0 = render_nine(render_disk(@towers[0][level]))
      tower1 = render_nine(render_disk(@towers[1][level]))
      tower2 = render_nine(render_disk(@towers[2][level]))
      puts "#{tower0}|#{tower1}|#{tower2}"

      level -= 1
    end
    puts " tower 0 | tower 1 | tower 2 "
    puts ""
  end

  def render_disk(disk_num)
    return ' ' if disk_num.nil?
    disks = ""
    (disk_num * 2 - 1).times { disks += "=" }
    disks
  end

  def render_nine(str)
    new_str = str
    until new_str.length > 8
      new_str = ' ' + new_str + ' '
    end

    new_str
  end

  def won?
    return true if @towers[1] == [3, 2, 1] || @towers[2] == [3, 2, 1]
    false
  end

  def valid_move?(from_tower, to_tower)
    return false if @towers[from_tower].empty?
    return true if @towers[to_tower].empty?
    return false if @towers[from_tower][-1] > @towers[to_tower][-1]
    true
  end

  def move(from_tower, to_tower)
    if valid_move?(from_tower, to_tower)
      @towers[to_tower].push(@towers[from_tower].pop)
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  game = TowersOfHanoi.new
  game.play
end
